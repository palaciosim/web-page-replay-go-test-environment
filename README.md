# Web Page Replay GO Test Environment

## Minimum System requirements

- [Vagrant](https://www.vagrantup.com) 2.2.4+
- [Virtualbox](https://www.virtualbox.org) 5.2+
- 8GB+ of RAM
- Virtualisation ( VT-X ) enabled in the BIOS ( Windows/Linux )
- Hyper-V turned off ( Windows )

## How To Use
To use it, download and install [Vagrant](https://www.vagrantup.com/) and [VirtualBox](https://www.virtualbox.org/). Then, clone this repository and run:

If this is your first time, please install the guest extension plugin:

```shell
vagrant plugin install vagrant-vbguest
```

To Start

```shell
cd web-page-replay-go-test-environment
```

Then run the test enviroment

```shell
vagrant up
```
## From Linux GUI enviroment virtual machine

```shell
user: vagrant
pass: vagrant
```
Before run the record mode and replay mode you have to install [Google Chrome Beta](https://www.google.com/intl/en/chrome/beta/) in the Linux GUI enviroment virtual machine.

Open LXTerminal (Use the command line).
To execute some of the commands in the terminal you have to use 'sudo'

### Web Page Replay install

```shell
sudo go get github.com/catapult-project/catapult/web_page_replay_go
```
You can then find this directory in:

```shell
cd go/src/github.com/catapult-project/catapult/web_page_replay_go
```
And then install the required packages:

```shell
sudo go get github.com/urfave/cli
```

```shell
sudo go get golang.org/x/net/http2
```

Then you can review and follow the official documentation: [Web Page Replay GO Readme](https://github.com/catapult-project/catapult/tree/master/web_page_replay_go)

